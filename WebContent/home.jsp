<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム画面</title>
	</head>

	<body>
		<div class="systems">掲示板システム</div>

		<div class="main-contents">

			<div class="errorMessages">
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if>
			</div>

			<div class="header">
				<c:if test="${ empty loginUser }">ログインしてください
					<a href="login">ログイン</a>
				</c:if>

				<c:if test="${ not empty loginUser }">
					<h2><c:out value="${loginUser.userName}" />でログイン中</h2>

					<div class="menu">
						<a href="newPost">新規投稿</a>
						<a href="management">ユーザー管理</a>
					</div><br /><br />

					<div class="search">検索機能<br /><br />

						<label for="category">カテゴリーを入力してください</label>
						<input name="categorySearch"  id="category"> <br />

						<label for="date">日付を選択してください</label>
						<input type="date" name="from" >～
						<input type="date" name="to"><br /><br />

						<input type="submit" value="検索" /> <br /><br />

						<div class="message">投稿一覧<br /><br />
							<c:forEach items="${messages}" var="message">
								<div class="title">件名:<c:out value="${message.title}" /></div>
								<div class="category">カテゴリー:<c:out value="${message.category}" /></div>
								<div class="text">本文:<c:out value="${message.text}" /></div>
								<div class="date">投稿日時:<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
								<div class="account-name">
									<span class="account">登録者:<c:out value="${message.userName}" /></span>
								</div><br />
							</c:forEach>
						</div>
					</div>
				</c:if>
			</div>
		</div>
	</body>
</html>