<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿</title>
	</head>
	<body>
		<div class="systems">掲示板システム</div>
		<a href="./">戻る</a><br /><br />

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="post-area">
			<form action="newPost" method="post">
				件名(30文字以下)<br />
				<textarea name="title" cols="60" rows="1" class="tweet-box"></textarea><br />
				カテゴリー(10文字以下)<br />
				<textarea name="category" cols="20" rows="1" class="tweet-box"></textarea><br />
				本文(1000文字以下)<br />
				<textarea name="text" cols="100" rows="20" class="tweet-box"></textarea><br />

				<br /> <input type="submit" value="投稿">
			</form>
		</div>


	</body>
</html>