<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
	</head>
	<body>
		<div class="systems">掲示板システム</div>

		<a href="./">戻る</a><br /><br />

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="menu">
			<a href="signup">ユーザー新規登録</a>
			<a href="edit">ユーザー編集</a>
		</div><br /><br />

		<div class="allUser">ユーザーの登録情報<br /><br />
			<c:forEach items="${allUserList}" var="allUserList">
				<div class="id">ID:<c:out value="${allUserList.id}" /></div>
				<div class="userName">ユーザー名:<c:out value="${allUserList.userName}" /></div>
				<div class="branch">支店:<c:out value="${allUserList.branch}" /></div>
				<div class="department">部署・役職:<c:out value="${allUserList.department}" /></div>
				<div class="loginId">ログインID:<c:out value="${allUserList.loginId}" /></div>
				<div class="createdDate">投稿日時:<fmt:formatDate value="${allUserList.createdDate}"
						pattern="yyyy/MM/dd HH:mm:ss" /></div><br />
			</c:forEach>
		</div>

	</body>
</html>