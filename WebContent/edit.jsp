<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="ContentType" content ="text/html; charset=UTF-8">
		<title>${loginUser.userName}の編集</title>
	</head>
	<body>
		<div class="main-contents">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<form action="edit" method="post"><br />
				<input name="id" value="${edituser.id}" id="id" type="hidden" />
				<label for="userName">ユーザー名</label>
				<input name="userName" value="${editUser.userName}" id="userName" /><br />

				<label for="branch">支店</label>
				<input name="branch" value="${editUser.branch}" id="branch" /><br />

				<label for="department">部署・役職</label>
				<input name="department" value="${editUser.department}" id="department" /><br />

				<label for="loginId">ログインID</label>
				<input name="loginId" value="${editUser.loginId}" id="loginId" /><br />

				<label for="password">パスワード</label>
				<input name="password" type="password" id="password" /><br />

				<input type="submit" value="編集" /> <br />
				<a href="./">戻る</a>
			</form>
			<div class="systems">掲示板システム</div>
		</div>
	</body>
</html>