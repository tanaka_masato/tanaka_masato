<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録</title>
</head>
<body>
	<div class="system">掲示板システム</div>

	<a href="./">戻る</a><br /><br />

	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="signup" method="post">

			<label for="userName">ユーザー名</label>
			<input name="userName" id="userName" /> <br />
			<label for="branch">支店</label>
			<select name="branchId">
				<c:forEach items="${branches}" var="branches">
					<option value="${branches.id}">${branches.branch}</option>
				</c:forEach>
			</select> <br />
			<label for="department">部署・役職</label>
			<select name="departmentId">
				<c:forEach items="${departments}" var="departments">
					<option value="${departments.id}">${departments.department}</option>
				</c:forEach>
			</select> <br />
			<label for="loginId">ログインID</label>
			<input name="loginId" id="loginId"> <br />
			<label for="password">パスワード</label>
			<input name="password" type="password" id="password"> <br />
			<label for="passwordConfimation">確認用パスワード</label>
			<input name="passwordConfimation" type="password" id="passwordConfimation"> <br /><br />

			<input type="submit" value="登録" />
		</form>

	</div>
</body>
</html>