package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		List<String> messages = new ArrayList<>();

		// ログインしてない場合⇒エラーメッセージの表示

		if (user == null) {
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);

			response.sendRedirect("login");

		// ログインしている人が本社総務部以外の場合⇒エラーメッセージの表示

		} else if (user.getDepartmentId() != 1 && user.getDepartmentId() != 2) {
			messages.add("この操作は本社総務部しか扱えません");
			session.setAttribute("errorMessages", messages);

			response.sendRedirect("./");

		// ログインしている人が本社総務部の場合

		} else {

			// ユーザーの登録情報を一覧表示
			List<User> allUserList = new UserService().getUser();
			request.setAttribute("allUserList", allUserList);
			request.getRequestDispatcher("management.jsp").forward(request, response);
		}
	}
}
