package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Search;
import beans.User;
import beans.UserPost;
import service.PostService;
import service.SearchService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		request.setAttribute("loginUser", user);

		// ログイン中の場合

		List<UserPost> messages = new PostService().getPost();
		request.setAttribute("messages", messages);

		request.getRequestDispatcher("home.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String category = request.getParameter("category");

		HttpSession session = request.getSession();
		SearchService searchService = new SearchService();
		Search search = searchService.search(category);

		// SearchServiceより検索された情報が取得できた場合

		if (search != null) {
			session.setAttribute("categorys", search);
			response.sendRedirect("./");
		}
	}
}
