package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		List<String> messages = new ArrayList<>();

		// ログインしてない場合⇒エラーメッセージの表示

		if (user == null) {
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);

			response.sendRedirect("login");

		// ログインしている人が本社総務部以外の場合⇒エラーメッセージの表示

		} else if (user.getDepartmentId() != 1 && user.getDepartmentId() != 2) {
			messages.add("この操作は本社総務部しか扱えません");
			session.setAttribute("errorMessages", messages);

			response.sendRedirect("./");

		// ログインしている人が本社総務部の場合

		} else {

			// BranchServiceとDepartmentServiceで受け取った情報をjspで表示

			List<User> branchList = new BranchService().findAll();
			List<User> departmentList = new DepartmentService().findAll();
			session.setAttribute("branches", branchList);
			session.setAttribute("departments", departmentList);

			// ユーザー登録画面へ遷移

			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<>();
		HttpSession session = request.getSession();
		User user = new User();

		/* jspに入力されたデータを取得しUserServiceへ
		 * 　⇒ユーザー管理画面へ遷移
		 */

		if (isValid(request,messages) == true) {

			user.setUserName(request.getParameter("userName"));
			user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
			user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
			user.setLoginId(request.getParameter("loginId"));
			user.setPassword(request.getParameter("password"));

			new UserService().register(user);

			response.sendRedirect("management");

		// バリデーションメッセージをjspに表示し、再度入力画面へ

		}else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("signup");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String userName = request.getParameter("userName");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordConfimation = request.getParameter("passwordConfimation");

		// 各エラーにおけるバリデーションメッセージの取得

		if (StringUtils.isEmpty(userName) == true) {
			messages.add ("ユーザー名を入力してください");
		} else if (userName.length() > 10) {
			messages.add("ユーザー名は10文字以下で入力してください");
		}

		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if (!loginId.matches("[a-zA-Z0-9]{6,20}")) {
			messages.add("ログインIDは半角英数字で6文字以上20文字以下で入力してください");
		}

		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		} else if (!password.matches("[ -~]{6,20}")) {
			messages.add("パスワードは半角文字で6文字以上20文字以下で入力してください");
		}

		if (StringUtils.isEmpty(passwordConfimation) == true) {
			messages.add("確認用パスワードを入力してください");
		} else if (!passwordConfimation.matches("[ -~]{6,20}")) {
			messages.add("確認用パスワードは半角文字で6文字以上20文字以下で入力してください");
		}

		if (!password.equals(passwordConfimation)) {
			messages.add("パスワードと確認用パスワードは一致させてください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
