package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import beans.User;
import service.PostService;

@WebServlet(urlPatterns = { "/newPost" })
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");

		// 	ログイン中⇒post.jspの表示

		if (user != null) {
			request.getRequestDispatcher("post.jsp").forward(request, response);

		// ログインしていない場合

		} else {
			List<String> messages = new ArrayList<>();
			messages.add("ログインしてください");
			session.setAttribute("errormessages", messages);

			response.sendRedirect("login");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		/* ログインユーザーから入力されたデータを取得しPostServiceへ
		 * 　⇒ホーム画面へ遷移
		 */

		if (isValid(request, messages) == true) {
			User user = (User) session.getAttribute("loginUser");

			Post post = new Post();
			post.setTitle(request.getParameter("title"));
			post.setCategory(request.getParameter("category"));
			post.setText(request.getParameter("text"));
			post.setUserId(user.getId());

			new PostService().register(post);

			response.sendRedirect("./");

			// バリデーションメッセージをjspに表示し、再度入力画面へ

		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("newPost");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String text = request.getParameter("text");

		// 各エラーにおけるバリデーションメッセージの取得

		if (StringUtils.isEmpty(title) == true) {
			messages.add("件名を入力してください");
		} else if (title.length() > 30) {
			messages.add("件名は30文字以下で入力してください");
		}

		if (StringUtils.isEmpty(category) == true) {
			messages.add("カテゴリーを入力してください");
		} else if (category.length() > 10) {
			messages.add("カテゴリーは10文字以下で入力してください");
		}

		if (StringUtils.isEmpty(text) == true) {
			messages.add("本文を入力してください");
		} else if (text.length() > 1000) {
			messages.add("本文は1000文字以下で入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
