package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class DepartmentDao {

	// DBから部署・役職に関する情報を取得

	public List<User> findAll(Connection connection) {

		PreparedStatement ps = null;

		try {

			String sql = "SELECT * FROM departments";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			List<User> ret = toUserDepartmentList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// 取得した情報をUserに入れる

	private List<User> toUserDepartmentList(ResultSet rs) throws SQLException {
		List<User> departmentList = new ArrayList<>();
		try {
			while(rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setDepartment(rs.getString("department"));
				departmentList.add(user);
			}
			return departmentList;
		} finally {
			close(rs);
		}
	}
}
