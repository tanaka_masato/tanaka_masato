package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Search;
import exception.SQLRuntimeException;

public class SearchDao {

	// DBから検索結果に当てはまる情報を取得

	public Search getSearch(Connection connection, String category) {

		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM posts WHERE category LIKE = ?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, category);

			ResultSet rs = ps.executeQuery();
			List<Search> searchList = toSearchList(rs);

			if (searchList.isEmpty() == true) {
				return null;
			} else {
				return searchList.get(0);
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

		// 取得した情報をSearchに入れる

	private List<Search> toSearchList(ResultSet rs) throws SQLException {
		List<Search> ret = new ArrayList<>();
		try {
			while(rs.next()) {
				String category = rs.getString("category");
				Search search = new Search();
				search.setCategory(category);
				ret.add(search);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
