package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserPost;
import exception.SQLRuntimeException;


public class UserPostDao {
	public List<UserPost> postJoinUser(Connection connection, int num) {

		// 投稿内容とユーザー情報を結合

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id as id, ");
			sql.append("posts.user_id as user_id, ");
			sql.append("posts.title as title, ");
			sql.append("posts.category as category, ");
			sql.append("posts.text as text, ");
			sql.append("users.user_name as user_name, ");
			sql.append("posts.created_date as created_date ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toUserPostList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserPost> toUserPostList(ResultSet rs)
			throws SQLException {

		// 取得した値をUserPostへ

		List<UserPost> ret = new ArrayList<UserPost>();
		try {
			while (rs.next()) {

				String userName = rs.getString("user_name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String title = rs.getString("title");
				String category = rs.getString("category");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserPost post = new UserPost();
				post.setUserName(userName);
				post.setId(id);
				post.setUserId(userId);
				post.setTitle(title);
				post.setCategory(category);
				post.setText(text);
				post.setCreatedDate(createdDate);

				ret.add(post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
