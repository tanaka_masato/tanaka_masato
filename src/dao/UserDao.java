package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class UserDao {

	// UsserServiceから受け取ったユーザー新規登録内容をDBに登録

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("user_name");
			sql.append(", branch_id");
			sql.append(", department_id");
			sql.append(", login_id");
			sql.append(", password");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getUserName());
			ps.setInt(2, user.getBranchId());
			ps.setInt(3, user.getDepartmentId());
			ps.setString(4, user.getLoginId());
			ps.setString(5, user.getPassword());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
    }

	// DBからログインIDとパスワード情報を元に、ユーザー登録情報を取得

	public User getUser(Connection connection,String loginId, String password) {
		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2,  password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// 取得したユーザー登録情報をUserに入れる

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String userName = rs.getString("user_name");
				int branchId = rs.getInt("branch_id");
				int departmentId = rs.getInt("department_id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				User user = new User();
				user.setId(id);
				user.setUserName(userName);
				user.setBranchId(branchId);
				user.setDepartmentId(departmentId);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	// DBから全ユーザーの情報を取得

	public List<User> userJoin(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.user_name as user_name, ");
			sql.append("users.branch_id as branch_id, ");
			sql.append("users.department_id as department_id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.created_date as created_date, ");
			sql.append("users.updated_date as updated_date, ");
			sql.append("branches.branch as branch, ");
			sql.append("departments.department as department ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id;");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<User> ret = toAllUserList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// 取得した全ユーザー情報をUserに入れる

	private List<User> toAllUserList(ResultSet rs) throws SQLException {
		List<User> ret = new ArrayList<>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String userName = rs.getString("user_name");
				String branch = rs.getString("branch");
				String department = rs.getString("department");
				String loginId = rs.getString("login_id");
				Timestamp createdDate= rs.getTimestamp("created_date");

				User user = new User();
				user.setId(id);
				user.setUserName(userName);
				user.setBranch(branch);;
				user.setDepartment(department);
				user.setLoginId(loginId);
				user.setCreatedDate(createdDate);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
