package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;


public class UserService {
	public void register(User user) {

		/* SignUpServletから受け取った情報をUserDaoへ
		 * 　⇒パスワードは暗号化してからUserDaoへ
		 */

		Connection connection = null;
		try {
			connection = getConnection();

			// パスワードの暗号化

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	// UserDaoで取得した全ユーザー情報を取得

	private static final int LIMIT_NUM = 1000;

	public List<User> getUser() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			List<User> allUserList = userDao.userJoin(connection,LIMIT_NUM);
			commit(connection);
			return allUserList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}