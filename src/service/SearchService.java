package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.Search;
import dao.SearchDao;

public class SearchService {

	/* HomeServletで取得した入力された検索情報ををSearchDaoへ
	 * 　⇒SearchDaoで取得した値をSearchrから受け取る
	 */

	public Search search(String category) {

		Connection connection = null;
		try {
			connection = getConnection();

			SearchDao searchDao = new SearchDao();
			Search search = searchDao.getSearch(connection, category);

			commit(connection);

			return search;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
